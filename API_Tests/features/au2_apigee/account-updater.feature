@intg
@tokens
Feature: Creating tokens
    As an API consumer
	I want to create, update and delete tokens
	So that I know token processing works correctly

	@get-account-updater
    Scenario: Verify the API proxy is responding
        Given I set clientId header to `clientId`
        And I set apiSecret to 'clientSecret'
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /accountupdater/v1/accountupdater?startDate=2017-08-01&endDate=2017-09-18
        Then response code should be 200
        And response header Content-Type should be application/json
        # And response body path $.environment should be dev
        # And response body path $.apiproxy should be `apiproxy`
        # And response body path $.VaultResults if of type Array

    @update-account-updater
    Scenario: Update a account-updater
        Given I have valid merchant credentials to PUT
        And I set content-type header to application/json
        And I set body to { "cardData":{ "number":"5454545454545454", "expiration":"1299" } }
        When I use HMAC and PUT to /accountupdater/v1/accountupdater?startDate=2017-08-01&endDate=2017-09-18
        Then the response code should be 404
        And the response header Content-Type should be application/json
        And put response body path $.code should be 100006
        And put response body path $.message should be Resource not found
        And put response body path $.detail should be There is no resource at the path specified

    @delete-account-updater
    Scenario: Delete a bankcard token
        Given I have valid merchant credentials to DELETE
        When I use HMAC and DELETE /accountupdater/v1/accountupdater?startDate=2017-08-01&endDate=2017-09-18
        Then delete response code should be 404
        And delete response header Content-Type should be application/json
        And delete response body path $.code should be 100006
        And delete response body path $.message should be Resource not found
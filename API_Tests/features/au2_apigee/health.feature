@intg
Feature: API proxy health
	As API administrator
    I want to monitor Apigee proxy and backend service health
    So I can alert when it is down

    #health testing
	@get-ping
    Scenario: Verify the API proxy is responding
        Given I set clientId header to `clientId`
        And I set apiSecret to 'clientSecret'
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /accountupdat er/v1/ping
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path environment should be qa
        And response body path apiproxy should be account-updater-v1
        And response body path client should be 184.156.48.176
        # And response body path $.latency should be ^\d{1,2}
        And response body path message should be PONG
        

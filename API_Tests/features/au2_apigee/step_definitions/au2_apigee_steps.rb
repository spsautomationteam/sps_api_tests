require_relative '../../support/API-tools'
require_relative '../../support/hmac-tools'
require 'cucumber'
require 'rspec'
require 'date'

##for QA
 file = File.read('../config/config.json')

##for PRE PROD
#file = File.read('../config/config-preprod.json')
data_hash = JSON.parse(file)
hmac = HmacTools.new
$host = data_hash['values'][0]['value']

Given(/^I set clientId header to `clientId`$/) do
  @clientId = data_hash['values'][5]['value']
  puts @clientId
end

Given(/^I set apiSecret to 'clientSecret'$/) do
  @apiSecret = data_hash['values'][6]['value']
end

Given(/^I set merchantId header to `merchantId`$/) do
  @merchantId = data_hash['values'][7]['value']
end

Given(/^I set merchantId value to (.*)$/) do |arg1|
  @merchantId = arg1
end

Given(/^I set merchantKey header to `merchantKey`$/) do
  @merchantKey = data_hash['values'][8]['value']
end

Given(/^I set Authorization header to invalid-hmac$/) do
  @authorization = "abc123"
end

Given(/^I set merchantKey value to (.*)$/) do |arg1|
  @merchantKey = arg1
end

Given(/^I set clientId value to (.*)$/) do |arg1|
  @clientId = arg1
end

Given(/^I set nonce header to (.*)$/) do |arg1|
  @nonce = arg1
end

Given(/^I set timestamp header to (.*)$/) do |arg1|
  @timestamp = arg1
end

# Given(/^I set authorization header to (.*)$/) do |arg1|
#   @merchantKey = arg1
# end





When(/^I use HMAC and GET (.*)/) do |arg1|
  url = $host + arg1
  puts url
  authorization = hmac.hmac(@apiSecret, 'GET', url, '', @merchantId, nonce = DateTime.now.strftime('%Q'), timestamp = DateTime.now.strftime('%Q').to_i/1000)
  puts authorization
  puts "#{@merchantKey} - TEST"
  @get1 = GetData.new($host, arg1, @clientId, @merchantId, @merchantKey, nonce, timestamp, authorization, "application/json")
end

When(/^I GET (.*)/) do |arg1|
  url = $host + arg1
  puts url
  authorization = hmac.hmac(@apiSecret, 'GET', url, '', @merchantId, @nonce, @timestamp)
  puts authorization
  @get1 = GetData.new($host, arg1, @clientId, @merchantId, @merchantKey, @nonce, @timestamp, authorization, "application/json")
end

When(/^I use invalid hmac and GET (.*)/) do |arg1|
  url = $host + arg1
  puts url
  authorization = hmac.hmac(@apiSecret, 'GET', url, '', '', nonce = DateTime.now.strftime('%Q'), timestamp = DateTime.now.strftime('%Q').to_i/1000)
  # authorization = "abc123"
  # puts authorization
  # timestamp = DateTime.now.strftime('%Q').to_i/1000
  # nonce = DateTime.now.strftime('%Q')
  puts "#{@merchantKey} - TEST"
  @get1 = GetData.new($host, arg1, @clientId, @merchantId, @merchantKey, nonce, timestamp, authorization, "application/json")
  puts @get1
end

Then(/^response code should be (\d+)$/) do |arg1|
  expect(@get1.response_details[1]).to eq(arg1.to_i)
end

Then(/^response body should be (.*)$/) do |arg1|
  puts @get1.response_details
  expect(@get1.response_details[0]).to eq(arg1)
end

Then(/^response header Content\-Type should be (.*)$/) do |arg1|
  expect(@get1.response_details[2]).to include(arg1)
end

Given(/^I have valid merchant credentials to PUT$/) do
  @clientId = data_hash['values'][5]['value']
  @merchantId = data_hash['values'][7]['value']
  @merchantKey = data_hash['values'][8]['value']
  @apiSecret = data_hash['values'][6]['value']
end

Given(/^I set content\-type header to (.*)$/) do |arg1|
  @contentType = arg1
end

Given(/^I set body to (.*)$/) do |arg1|
  @body = arg1
end

When(/^I use HMAC and PUT to (.*)$/) do |arg1|
  authorization = hmac.hmac(@apiSecret, 'PUT', url = $host + arg1, @body, @merchantId, nonce = DateTime.now.strftime('%Q'), timestamp = DateTime.now.strftime('%Q').to_i/1000)
  @putData = PutData.new($host, arg1, @clientId, @merchantId, @merchantKey, nonce, timestamp, authorization, @contentType, @body)
end

Then(/^the response code should be (\d+)$/) do |arg1|
  expect(@putData.response_code).to eq(arg1.to_i)
end

Then(/^the response header Content-Type should be (.*)$/) do |arg1|
  puts @putData.content_type
  expect(@putData.content_type).to eq(arg1)
end

Then(/^response body path \$\.message should be (.*)$/) do |arg1|
  expect(@get1.response_details[4]).to eq(arg1)
end

Then(/^response body path \$\.detail should be (.*)$/) do |arg1|
  expect(@get1.response_details[5]).to eq(arg1)
end

Then(/^response body path \$\.code should be (\d+)$/) do |arg1|
  expect(@get1.response_details[3]).to eq(arg1.to_s)
end

Then(/^put response body path \$\.message should be (.*)$/) do |arg1|
  expect(@putData.message).to eq(arg1)
end

Then(/^put response body path \$\.detail should be (.*)$/) do |arg1|
  expect(@putData.detail).to eq(arg1)
end

Then(/^put response body path \$\.code should be (\d+)$/) do |arg1|
  expect(@putData.code).to eq(arg1.to_s)
end

Given(/^I have valid merchant credentials to DELETE$/) do
  @clientId = data_hash['values'][5]['value']
  @merchantId = data_hash['values'][7]['value']
  @merchantKey = data_hash['values'][8]['value']
  @apiSecret = data_hash['values'][6]['value']
  @body = ''
end

When(/^I use HMAC and DELETE (.*)$/) do |arg1|
  authorization = hmac.hmac(@apiSecret, 'DELETE', url = $host + arg1, @body, @merchantId, nonce = DateTime.now.strftime('%Q'), timestamp = DateTime.now.strftime('%Q').to_i/1000)
  @deleteData = DeleteData.new($host, arg1, @clientId, @merchantId, @merchantKey, nonce, timestamp, authorization, @contentType)
end

Then(/^delete response code should be (\d+)$/) do |arg1|
  expect(@deleteData.response_code).to eq(arg1.to_i)
end

Then(/^delete response header Content-Type should be (.*)$/) do |arg1|
  expect(@deleteData.content_type).to eq(arg1)
end

Then(/^delete response body path \$\.code should be (\d+)$/) do |arg1|
  expect(@deleteData.code).to eq(arg1.to_s)
end
Then(/^delete response body path \$\.message should be (.*)$/) do |arg1|
  expect(@deleteData.message).to eq(arg1)
end
Then(/^delete response body path \$\.detail should be (.*)$/) do |arg1|
  expect(@deleteData.detail).to eq(arg1)
end

#for health.feature
Then(/^response body path (.*) should be (.*)$/) do |arg1, arg2|
  case
    when arg1 == 'client'
      expect(@get1.response_details[6]).to eq(arg2)
    when arg1 == 'environment'
      expect(@get1.response_details[7]).to eq(arg2)
    when arg1 == 'apiproxy'
      expect(@get1.response_details[8]).to eq(arg2)
  end
end
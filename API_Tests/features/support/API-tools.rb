require 'rest-client'
require 'json'
require_relative 'hmac-tools'
require 'open-uri'

#Get data request
class GetData
  attr_accessor :url, :get_url, :clientId, :merchantId, :merchantKey, :nonce, :timestamp, :authorization, :contenttype
  def initialize (url, get_url,clientId,merchantId, merchantKey, nonce, timestamp, authorization, contenttype)
    @url = url.to_s
    @uri = get_url.to_s
    @clientId = clientId.to_s
    @merchantId = merchantId.to_s
    @merchantKey = merchantKey.to_s
    @nonce = nonce.to_s
    @timestamp = timestamp.to_s
    @authorization = authorization
    @contenttype = contenttype.to_s
  end
  def response_details
    response = RestClient.get("#{@url}#{@uri}", headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization}){|response, request, result| response }
    puts response
    headers = response.headers
    data1 = response
    data2 = JSON.parse(data1)
    return response, response.code,headers[:content_type],data2['code'],data2['message'],data2['detail'],data2['client'],data2['environment'],data2['apiproxy']
  end
end

class PutData
  attr_accessor :url, :put_url, :clientId, :merchantId, :merchantKey, :nonce, :timestamp, :authorization, :contenttype, :body
  def initialize (url, put_url,clientId,merchantId, merchantKey, nonce, timestamp, authorization, contenttype, body)
    @url = url.to_s
    @uri = put_url.to_s
    @clientId = clientId.to_s
    @merchantId = merchantId.to_s
    @merchantKey = merchantKey.to_s
    @nonce = nonce.to_s
    @timestamp = timestamp.to_s
    @authorization = authorization
    @contenttype = contenttype.to_s
    @body = body.to_s
  end

  def putAPIData
    begin
      response = RestClient.put("#{@url}#{@uri}", body, headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization, :'Content-Type' => contenttype})
      puts "#{response.code}"
      puts "Response status: #{response}"

    rescue RestClient::BadRequest => e
      RestClient.log = e
      puts RestClient.log
      return e.response

    rescue RestClient::Unauthorized => e
      RestClient.log = e
      puts RestClient.log
      return e.response
    rescue RestClient::MethodNotAllowed => e
      RestClient.log = e
      puts RestClient.log
      return e.response
    rescue RestClient::NotFound => e
      RestClient.log = e
      puts RestClient.log
      return e.response
    end
  end
  def response_code
    begin
      response = RestClient.put("#{@url}#{@uri}", body, headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization, :'Content-Type' => contenttype})
      return response.code
    rescue => e
      return e.http_code
    end
  end

  def content_type
    begin
      response = RestClient.put("#{@url}#{@uri}", body, headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization, :'Content-Type' => contenttype})
      headers = response.headers
      return headers[:content_type]
    rescue => e
      return e.http_headers[:content_type]
    end
  end

  def message
    begin
      response = RestClient.put("#{@url}#{@uri}", body, headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization, :'Content-Type' => contenttype})
      data1 = response
      data2 = JSON.parse(data1)
      return data2['message']
    rescue => e
      data1 = e.response
      data2 = JSON.parse(data1)
      return data2['message']
    end
  end
  def code
    begin
      response = RestClient.put("#{@url}#{@uri}", body, headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization, :'Content-Type' => contenttype})
      data1 = response
      data2 = JSON.parse(data1)
      return data2['code']
    rescue => e
      data1 = e.response
      data2 = JSON.parse(data1)
      return data2['code']
    end
  end
  def detail
    begin
      response = RestClient.put("#{@url}#{@uri}",body, headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization, :'Content-Type' => contenttype}){|response, request, result| response }
      data1 = response
      data2 = JSON.parse(data1)
      return data2['detail']
    rescue => e
      data1 = e.response
      data2 = JSON.parse(data1)
      return data2['detail']
    end
  end
end

class DeleteData
  attr_accessor :url, :get_url, :clientId, :merchantId, :merchantKey, :nonce, :timestamp, :authorization, :contenttype
  def initialize (url, get_url,clientId,merchantId, merchantKey, nonce, timestamp, authorization, contenttype)
    @url = url.to_s
    @uri = get_url.to_s
    @clientId = clientId.to_s
    @merchantId = merchantId.to_s
    @merchantKey = merchantKey.to_s
    @nonce = nonce.to_s
    @timestamp = timestamp.to_s
    @authorization = authorization
    @contenttype = contenttype.to_s
  end


  def deleteAPIData
    begin
      response = RestClient.delete("#{@url}#{@uri}", headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization}){|response, request, result| response }
      puts "#{response.code}"
      puts "Response status: #{response}"

    rescue RestClient::BadRequest => e
      RestClient.log = e
      puts RestClient.log
      return e.response
    rescue RestClient::Unauthorized => e
      RestClient.log = e
      puts RestClient.log
      return e.response
    rescue RestClient::MethodNotAllowed => e
      RestClient.log = e
      puts RestClient.log
      return e.response
    end
  end

  def response_code
    begin
      response = RestClient.delete("#{@url}#{@uri}", headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization}){|response, request, result| response }
      return response.code
    rescue => e
      return e.http_code
    end
  end

  def content_type
    begin
      response = RestClient.delete("#{@url}#{@uri}", headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization}){|response, request, result| response }
      headers = response.headers
      return headers[:content_type]
    rescue => e
      return e.http_headers[:content_type]
    end
  end

  def code
    begin
      response = RestClient.delete("#{@url}#{@uri}", headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization}){|response, request, result| response }
      data1 = response
      data2 = JSON.parse(data1)
      return data2['code']
    rescue => e
      data1 = e.response
      data2 = JSON.parse(data1)
      return data2['code']
    end
  end
  def message
    begin
      response = RestClient.delete("#{@url}#{@uri}", headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization}){|response, request, result| response }
      data1 = response
      data2 = JSON.parse(data1)
      return data2['message']
    rescue => e
      data1 = e.response
      data2 = JSON.parse(data1)
      return data2['message']
    end
  end
  def detail
    begin
      response = RestClient.delete("#{@url}#{@uri}", headers = {:clientId => clientId, :merchantId => merchantId, :merchantKey => merchantKey, :nonce => nonce, :timestamp => timestamp, :Authorization => authorization}){|response, request, result| response }
      data1 = response
      data2 = JSON.parse(data1)
      return data2['detail']
    rescue => e
      data1 = e.response
      data2 = JSON.parse(data1)
      return data2['detail']
    end
  end
end

hmac = HmacTools.new()
url = 'https://api-qa.sagepayments.com'
uri = '/accountupdater/v1/accountupdater?startDate=2017-08-01&endDate=2017-09-18'
clientId = 'YF4GPWGEfYZmPtb5RWbZAXNGRV3M3G1T'
clientSecret = 'rgpGKka7GTlKixyS'
merchantId = '999999999997'
merchantKey = 'K3QD6YWYHFD'
body = ''
contentType = 'application/json'
nonce = DateTime.now.strftime('%Q')
timestamp = DateTime.now.strftime('%Q').to_i/1000
authorization = hmac.hmac(clientSecret, 'DELETE', url + uri, body, merchantId, nonce, timestamp)
delete1 = DeleteData.new(url, uri, clientId, merchantId, merchantKey, nonce, timestamp, authorization, contentType)

#puts delete1.message

#puts put1.code

#data = put1.putAPIData

#dataHash = JSON.parse(data)

#puts dataHash['message']